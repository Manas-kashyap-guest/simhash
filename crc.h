/*
 * Copyright (c) 2005-2007 Bart Massey
 * ALL RIGHTS RESERVED
 * Please see the file COPYING in this directory for license information.
 */

extern uint32_t hash_crc32(unsigned char *buf, int i0, int nbuf);
