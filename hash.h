/*
 * Copyright (c) 2005-2007 Bart Massey
 * ALL RIGHTS RESERVED
 * Please see the file COPYING in this directory for license information.
 */

extern void hash_reset(int);
extern int hash_contains(uint32_t);
extern void hash_insert(uint32_t);
extern int hash_delete(uint32_t);
